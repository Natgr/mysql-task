# **Database Design Task**  

This Task requires to create the database schema in order to keep track of our Music and Movies that we have purchased.
It is also required to populate the database with some sample data and queries that would answer some questions that would be of use for the design chosen.


## Option 1 - Music and Movies ##

## Name of the Database
`MusicMovies`

## Tables created

* Music

* Movies

* Shops 

* PurchaseDetails 



## Sample Queries

**What is the name of the song with the PurchaseID of PRMU4?**

select TitleMusic from Music where PurchaseID="PRMU4";


**What were the two purchases in 2014?**

select TitleMovie, TitleMusic from Music, Movies where YearMovie = "2014" and YearMusic= "2014";


**What is the name of the movie where Chris Hemsworth is the main actor?**

select TitleMovie from Movies where Actor="Chris Hemsworth";


**In what year did Adele release "Rolling in the Deep"?**

select YearMusic from Music where TitleMusic="Rolling in the Deep";

**On which date and time (put them in the right format dd/mm/yyyy hh:mm) did the user buy movies from Oxford Street.**

select DATE_FORMAT(str_to_date(`DateTime`,"%Y-%m-%d %H:%i:%s"), "%d/%m/%Y %H:%i") as `Date and Time` 
from PurchaseDetails 
where PurchaseLocation = "Oxford Street" and PurchaseType = "Movie";

**Display the datetime field in two separate fields called Date and Time. Make sure that the format is dd/mm/yyyy for the date and hh:mm for the time.**

select DATE_FORMAT(str_to_date(`DateTime`,"%Y-%m-%d %H:%i:%s"), "%d/%m/%Y") as `Date`,
DATE_FORMAT(str_to_date(`DateTime`,"%Y-%m-%d %H:%i:%s"), "%H:%i") as `Time` 
from PurchaseDetails;

**Find out how much money the user has spent on Amazon purchases.**

select CONCAT("Total = ",cast(Sum(PurchaseDetails.AmountPaid)as DECIMAL(4,2))) as Price from PurchaseDetails 
join Shops on PurchaseDetails.ShopID = Shops.ShopID
where ShopName="Amazon";


- - -

## Authors

* **Pranay Mistri**  
* **Natalia Christofi**  



