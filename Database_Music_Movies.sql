DROP DATABASE IF EXISTS MusicMovies;

CREATE DATABASE MusicMovies;

USE MusicMovies;

DROP TABLE IF EXISTS Music;
DROP TABLE IF EXISTS Movies;
DROP TABLE IF EXISTS Shops;
DROP TABLE IF EXISTS Purchase_details;

CREATE TABLE Music
(MusicID CHAR(5) NOT NULL, 
PurchaseID CHAR(5) NOT NULL,
TitleMusic VARCHAR(50) NOT NULL, 
YearMusic smallint NULL, 
GenreMusic VARCHAR(30) NULL, 
Artist VARCHAR(50) NULL,  
PRIMARY KEY(MusicID)
-- FOREIGN KEY (PurchaseID) REFERENCES PurchaseDetails(PurchaseID)
);


CREATE TABLE Movies
(MoviesID CHAR(5) NOT NULL, 
PurchaseID CHAR(5) NOT NULL,
TitleMovie VARCHAR(50) NOT NULL, 
YearMovie smallint NULL, 
GenreMovie VARCHAR(30) NULL, 
Actor VARCHAR(50) NULL, 
Synopsis TEXT NULL, 
PRIMARY KEY(MoviesID)
-- FOREIGN KEY (PurchaseID) REFERENCES PurchaseDetails(PurchaseID)
);


CREATE TABLE Shops
(ShopID CHAR(5) NOT NULL, 
ShopName VARCHAR(20) NOT NULL,  
ShopAddress VARCHAR(60) NULL, 
ShopPhone VARCHAR(24) NULL, 
PRIMARY KEY(ShopID));

CREATE TABLE PurchaseDetails
(PurchaseID CHAR(5) NOT NULL, 
PurchaseType VARCHAR(10) NOT NULL,  
PurchaseLocation VARCHAR(2083) NOT NULL,  
ShopID CHAR(5) NULL,
DateTime DATETIME NULL,
AmountPaid DECIMAL(19, 4) NULL, 
PRIMARY KEY(PurchaseID));
