use MusicMovies;

insert into Movies values("MV001", "PRMV1", "Pirates of the Carribean", "2014", "Sci-Fi", "Johnny Depp", "The first in a long sequel showing the battle between two groups of pirates");
insert into Movies values("MV002", "PRMV2", "The Dark Knight", "2008", "Action", "Christian Bale", "With the help of allies Lt. Jim Gordon (Gary Oldman) and DA Harvey Dent (Aaron Eckhart), Batman (Christian Bale) has been able to keep a tight lid on crime in Gotham City.");
insert into Movies values("MV003", "PRMV3", "Titanic", "1997", "Rom-Com", "Leanardo Di Caprio", "James Cameron's Titanic is an epic, action-packed romance set against the ill-fated maiden voyage of the R.M.S. Titanic; the pride and joy of the White Star Line and, at the time, the largest moving object ever built.");
insert into Movies values("MV004", "PRMV4", "The Avengers", "2012", "Action", "Chris Hemsworth", "When Thor's evil brother, Loki (Tom Hiddleston), gains access to the unlimited power of the energy cube called the Tesseract, Nick Fury (Samuel L. Jackson), director of S.H.I.E.L.D., initiates a superhero recruitment effort to defeat the unprecedented threat to Earth.");

insert into Music values("MU001", "PRMU1", "Thriller", "1997", "Hip-Hop", "Michael Jackson");
insert into Music values("MU002", "PRMU2", "Rolling in the Deep", "2013", "Pop", "Adele");
insert into Music values("MU003", "PRMU3", "Not Afraid", "2014", "Rap", "Eminem");
insert into Music values("MU004", "PRMU4", "Hotline Bling", "2016", "Pop", "Drake");

insert into Shops values("SH001", "Amazon", "Canada Square", "07467272554");
insert into Shops values("SH002", "Spotify", "Regent Street", "02342155644");
insert into Shops values("SH003", "BlockBuster", "Oxford Street", "02333155044");
insert into Shops values("SH004", "HMV", "Leicester Square", "02328155002");

insert into PurchaseDetails values("PRMV1", "Movie", "https://www.amazon.co.uk/b/?node=3046737031&ie=UTF8/", "SH001", "2019-09-10 10:05:23", "11.99");
insert into PurchaseDetails values("PRMU1", "Music", "https://www.spotify.co.uk", "SH002", "2019-08-23 22:30:23", "4.99");
insert into PurchaseDetails values("PRMV2", "Movie", "Oxford Street", "SH003", "2019-04-02 14:31:12", "15.99");
insert into PurchaseDetails values("PRMU2", "Music", "https://www.amazon.co.uk/b/?node=3046737031&ie=UTF8", "SH001", "2019-04-02 21:29:43", "4.99");
insert into PurchaseDetails values("PRMV3", "Movie", "Leicester Square", "SH004", "2019-05-06 20:04:15", "9.55");
insert into PurchaseDetails values("PRMU3", "Music", "https://www.spotify.co.uk", "SH002", "2019-02-14 10:45:10", "3.99");
insert into PurchaseDetails values("PRMV4", "Movie", "Oxford Street", "SH003", "2019-09-10 08:05:12", "13.85");
insert into PurchaseDetails values("PRMU4", "Music", "https://www.amazon.co.uk/b/?node=3046737031&ie=UTF8/", "SH001", "2019-04-01 00:30:23", "7.99");
